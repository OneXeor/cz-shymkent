package onexeor.com.searchjob;

import android.app.Dialog;
import android.graphics.Color;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import onexeor.com.searchjob.fragments.SignInFragments.fragments.applicantFragments.FragmentProfileApplicant;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.applicantFragments.FragmentVacansyListOfApplicant;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments.FragmentProfileEmployer;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments.FragmentVacansyListOfEmployer;

public class MainActivity extends AppCompatActivity {

    private BottomBar bottomBar_appl, bottomBar_emp;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        bottomBar_appl = (BottomBar) findViewById(R.id.bottomBar_appl);
        bottomBar_emp = (BottomBar) findViewById(R.id.bottomBar_emp);


        if (Helper.getSPreferenceLogin(getApplicationContext()).getInt(Constants.PREFS_LOGIN_TYPE_USER, 0) == 0) {
            bottomBar_appl.setOnTabSelectListener(new OnTabSelectListener() {
                @Override
                public void onTabSelected(@IdRes int tabId) {
                    Log.e("select_list", "selected " + tabId);
                    usualFragments();
                    switch (tabId) {
                        case R.id.vacancies_appl:
                            Helper.jumpFragment(R.id.container, getSupportFragmentManager(), FragmentVacansyListOfApplicant.newInstance(), false).commit();
                            break;
                        case R.id.profile_appl:
                            Helper.jumpFragment(R.id.container, getSupportFragmentManager(), FragmentProfileApplicant.newInstance(), false).commit();
                            break;
                    }
                }
            });
            bottomBar_emp.setVisibility(View.GONE);
            bottomBar_appl.setVisibility(View.VISIBLE);
            bottomBar_appl.selectTabAtPosition(0);
        } else {
            bottomBar_emp.setOnTabSelectListener(new OnTabSelectListener() {
                @Override
                public void onTabSelected(@IdRes int tabId) {
                    usualFragments();
                    Log.e("select_list", "selected " + tabId);
                    switch (tabId) {
                        case R.id.vacancies_emp:
                            Helper.jumpFragment(R.id.container, getSupportFragmentManager(), FragmentVacansyListOfEmployer.newInstance(), false).commit();
                            break;
                        case R.id.profile_emp:
                            Helper.jumpFragment(R.id.container, getSupportFragmentManager(), FragmentProfileEmployer.newInstance(), false).commit();
                            break;
                    }
                }
            });
            bottomBar_emp.setVisibility(View.VISIBLE);
            bottomBar_appl.setVisibility(View.GONE);
            bottomBar_appl.selectTabAtPosition(0);
        }

        func(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    //Смена кнопок "burger" и "home" в зависимости от стека фрагментов в менеджере
    private void func(final Toolbar toolbar) {
        if (toolbar != null) {
            getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        if (getSupportActionBar() != null)
                            getSupportActionBar().setDisplayHomeAsUpEnabled(true); // show back button
                        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onBackPressed();
                            }
                        });
                    } else {
                        usualFragments();
                    }
                }
            });
        }
    }

    private void usualFragments() {
        int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackCount != 0)
            for (int i = 0; i < backStackCount; i++) {

                int backStackId = getSupportFragmentManager().getBackStackEntryAt(i).getId();

                getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            }
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);

    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            onCreateDialog().show();
        } else {
            super.onBackPressed();
        }
    }

    //Диалог выхода из приложения
    public Dialog onCreateDialog() {
        return new MaterialDialog.Builder(this)
                .title(R.string.exit)
                .positiveText(R.string.yes)
                .content(R.string.you_sure_want_exit)
                .theme(Theme.LIGHT)
                .negativeText(R.string.no)
                .negativeColor(Color.parseColor("#8b000000"))
                .positiveColor(getResources().getColor(R.color.colorAccent))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finishAffinity();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                }).build();
    }
}
