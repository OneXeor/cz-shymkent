package onexeor.com.searchjob.api.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by OneXeor on 25.01.17
 * In 11:50.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Vacancy implements Parcelable {

    private String id;
    private String date;
    private String date_gmt;
    private String activity;
    private String time_interval;
    private String salary;
    private String phone;
    private String work_date;
    private String workplaces;
    private String address_go;
    private Title title;
    private Content content;

    public Vacancy() {
    }

    public String getPhone() {
        return phone;
    }

    public String getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getDate_gmt() {
        return date_gmt;
    }

    public String getActivity() {
        return activity;
    }

    public String getTime_interval() {
        return time_interval;
    }

    public String getSalary() {
        return salary;
    }

    public String getWork_date() {
        return work_date;
    }

    public String getWorkplaces() {
        return workplaces;
    }

    public String getAddress_go() {
        return address_go;
    }

    public Title getTitle() {
        return title;
    }

    public Content getContent() {
        return content;
    }

    public Vacancy(String id, String date, String date_gmt, String activity, String time_interval, String salary, String work_date, String workplaces, String address_go, Title title, Content content) {
        this.id = id;
        this.date = date;
        this.date_gmt = date_gmt;
        this.activity = activity;
        this.time_interval = time_interval;
        this.salary = salary;
        this.work_date = work_date;
        this.workplaces = workplaces;
        this.address_go = address_go;
        this.title = title;
        this.content = content;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Title implements Parcelable {
        private String rendered;

        public Title(String rendered) {
            this.rendered = rendered;
        }

        public Title() {
        }

        public String getRendered() {
            return rendered;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.rendered);
        }

        protected Title(Parcel in) {
            this.rendered = in.readString();
        }

        public final Parcelable.Creator<Title> CREATOR = new Parcelable.Creator<Title>() {
            @Override
            public Title createFromParcel(Parcel source) {
                return new Title(source);
            }

            @Override
            public Title[] newArray(int size) {
                return new Title[size];
            }
        };
    }
@JsonIgnoreProperties(ignoreUnknown = true)
    public class Content implements Parcelable {
        private String rendered;

        public Content(String rendered) {
            this.rendered = rendered;
        }

        public Content() {
        }

        public String getRendered() {
            return rendered;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.rendered);
        }

        public Content(Parcel in) {
            this.rendered = in.readString();
        }

        public final Parcelable.Creator<Content> CREATOR = new Parcelable.Creator<Content>() {
            @Override
            public Content createFromParcel(Parcel source) {
                return new Content(source);
            }

            @Override
            public Content[] newArray(int size) {
                return new Content[size];
            }
        };
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.date);
        dest.writeString(this.date_gmt);
        dest.writeString(this.activity);
        dest.writeString(this.time_interval);
        dest.writeString(this.salary);
        dest.writeString(this.phone);
        dest.writeString(this.work_date);
        dest.writeString(this.workplaces);
        dest.writeString(this.address_go);
        dest.writeParcelable(this.title, flags);
        dest.writeParcelable(this.content, flags);
    }

    protected Vacancy(Parcel in) {
        this.id = in.readString();
        this.date = in.readString();
        this.date_gmt = in.readString();
        this.activity = in.readString();
        this.time_interval = in.readString();
        this.salary = in.readString();
        this.phone = in.readString();
        this.work_date = in.readString();
        this.workplaces = in.readString();
        this.address_go = in.readString();
        this.title = in.readParcelable(Title.class.getClassLoader());
        this.content = in.readParcelable(Content.class.getClassLoader());
    }

    public static final Creator<Vacancy> CREATOR = new Creator<Vacancy>() {
        @Override
        public Vacancy createFromParcel(Parcel source) {
            return new Vacancy(source);
        }

        @Override
        public Vacancy[] newArray(int size) {
            return new Vacancy[size];
        }
    };
}
