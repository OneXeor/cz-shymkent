package onexeor.com.searchjob.api;

import android.util.Base64;
import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by OneXeor on 23.01.17
 * In 9:38.
 */

public class ServiceGenerator {

    public static final String API_BASE_URL = "http://c18415.shared.hc.ru/admin/wp-json/job-exchange/v2/";
    public static final String API_VAC_URL = "http://c18415.shared.hc.ru/admin/wp-json/wp/v2/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(JacksonConverterFactory.create());

    private static Retrofit.Builder builder_vac =
            new Retrofit.Builder()
                    .baseUrl(API_VAC_URL)
                    .addConverterFactory(JacksonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass, boolean isVacancy) {
        String credentials = "rest-admin" + ":" + "gP6@y6DRnxfeS^&POp8qOoLG";
        final String basic =
                "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", basic)
                        .header("Accept", "application/json")
                        .method(original.method(), original.body());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        Retrofit retrofit;
        OkHttpClient client = httpClient.build();
        if (isVacancy) {
            retrofit = builder_vac.client(client).build();

        } else
            retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }

    public static <S> S createService(Class<S> serviceClass) {
        String credentials = "rest-admin" + ":" + "gP6@y6DRnxfeS^&POp8qOoLG";
        final String basic =
                "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", basic)
                        .header("Accept", "application/json")
                        .method(original.method(), original.body());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        Retrofit retrofit;
        OkHttpClient client = httpClient.build();
        retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }
    public static <S> S createServices(Class<S> serviceClass, String phone, String password) {
        String credentials = phone + ":" + password;
        final String basic =
                "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

        Log.e("BASIC", basic);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", basic)
                        .header("Accept", "application/json")
                        .method(original.method(), original.body());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        Retrofit retrofit;
        OkHttpClient client = httpClient.build();
        retrofit = builder_vac.client(client).build();
        return retrofit.create(serviceClass);
    }
}
