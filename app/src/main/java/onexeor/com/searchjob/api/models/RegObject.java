package onexeor.com.searchjob.api.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;

/**
 * Created by OneXeor on 23.01.17
 * In 9:30.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegObject implements Parcelable {

    private String code;
    private String result;
    private String message;
    private Data data;
    private int _id;
    private String username;
    private String name;
    private String first_name;
    private String description;
    private String last_name;
    private String email;
    private String account_type;
    private String phone;
    private String age;
    private String inn;
    private String activity;
    private String legal_status;
    private String company_name;
    private String link;
    private String[] roles;
    private String registered_date;
    private HashMap<String, String> avatar_urls;

    public String getAccount_type() {
        return account_type;
    }

    public String getPhone() {
        return phone;
    }

    public String getAge() {
        return age;
    }

    public String getInn() {
        return inn;
    }

    public String getActivity() {
        return activity;
    }

    public String getLegal_status() {
        return legal_status;
    }

    public static Creator<RegObject> getCREATOR() {
        return CREATOR;
    }

    public String getDescription() {
        return description;
    }

    public RegObject(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public RegObject(int _id, String username, String name, String first_name, String last_name, String email, String link, String[] roles, String registered_date, HashMap<String, String> avatar_urls) {
        this._id = _id;
        this.username = username;
        this.name = name;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.link = link;
        this.roles = roles;
        this.registered_date = registered_date;
        this.avatar_urls = avatar_urls;
    }

    public String getCompany_name() {
        return company_name;
    }

    public String getResult() {
        return result;
    }

    public int get_id() {
        return _id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getEmail() {
        return email;
    }

    public String getLink() {
        return link;
    }

    public String[] getRoles() {
        return roles;
    }

    public String getRegistered_date() {
        return registered_date;
    }

    public HashMap<String, String> getAvatar_urls() {
        return avatar_urls;
    }


    public RegObject() {
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public RegObject(String code, String message, Data data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.code);
        dest.writeString(this.result);
        dest.writeString(this.message);
        dest.writeParcelable(this.data, flags);
        dest.writeInt(this._id);
        dest.writeString(this.username);
        dest.writeString(this.name);
        dest.writeString(this.first_name);
        dest.writeString(this.description);
        dest.writeString(this.last_name);
        dest.writeString(this.email);
        dest.writeString(this.account_type);
        dest.writeString(this.phone);
        dest.writeString(this.age);
        dest.writeString(this.inn);
        dest.writeString(this.activity);
        dest.writeString(this.legal_status);
        dest.writeString(this.company_name);
        dest.writeString(this.link);
        dest.writeStringArray(this.roles);
        dest.writeString(this.registered_date);
        dest.writeSerializable(this.avatar_urls);
    }

    protected RegObject(Parcel in) {
        this.code = in.readString();
        this.result = in.readString();
        this.message = in.readString();
        this.data = in.readParcelable(Data.class.getClassLoader());
        this._id = in.readInt();
        this.username = in.readString();
        this.name = in.readString();
        this.first_name = in.readString();
        this.description = in.readString();
        this.last_name = in.readString();
        this.email = in.readString();
        this.account_type = in.readString();
        this.phone = in.readString();
        this.age = in.readString();
        this.inn = in.readString();
        this.activity = in.readString();
        this.legal_status = in.readString();
        this.company_name = in.readString();
        this.link = in.readString();
        this.roles = in.createStringArray();
        this.registered_date = in.readString();
        this.avatar_urls = (HashMap<String, String>) in.readSerializable();
    }

    public static final Creator<RegObject> CREATOR = new Creator<RegObject>() {
        @Override
        public RegObject createFromParcel(Parcel source) {
            return new RegObject(source);
        }

        @Override
        public RegObject[] newArray(int size) {
            return new RegObject[size];
        }
    };
}
