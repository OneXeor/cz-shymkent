package onexeor.com.searchjob.api;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import onexeor.com.searchjob.api.models.RegObject;
import onexeor.com.searchjob.api.models.UserObject;
import onexeor.com.searchjob.api.models.Vacancy;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by OneXeor on 23.01.17
 * In 9:28.
 */

public interface Scheme {

    @FormUrlEncoded
    @POST("registration/step_1")
    Call<RegObject> register(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("registration/verify_resend/")
    Call<RegObject> resendCode(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("registration/step_2")
    Call<RegObject> register_two(@Field("user_id") String user_id, @Field("sms_code") String sms_code);

    @FormUrlEncoded

    @POST("registration/step_3/")
    Call<RegObject> register_three(@Field("phone") String phone, @Field("password") String password);

    @FormUrlEncoded
    @POST("users/{USER_ID}")
    Call<RegObject> edit_anket(@Path("USER_ID") String USER_ID, @Field("account_type") String account_type, @Field("phone") String phone,
                               @Field("age") String age, @Field("inn") String inn,
                               @Field("activity") String activity, @Field("legal_status") boolean legal_status,
                               @Field("company_name") String company_name, @Field("description") String description);

    @GET("users/{USER_ID}")
    Call<RegObject> getAnket(@Path("USER_ID") String USER_ID);

    @GET("vacancy")
    Call<List<Vacancy>> getVacancies();

    @DELETE("vacancy/{VACANCY_ID}")
    Call<RegObject> deleteVacancy(@Path("VACANCY_ID") String VACANCY_ID);


    @FormUrlEncoded
    @POST("vacancy")
    Call<Vacancy> addVacancy(@Field("activity") String activity, @Field("time_interval") String time_interval,
                             @Field("salary") String salary, @Field("work_date") String work_date, @Field("workplaces") String workplaces,
                             @Field("address_go") String address_go, @Field("status") String status, @Field("title") String title,
                             @Field("content") String content);

    @FormUrlEncoded
    @PUT("vacancy/{VACANCY_ID}")
    Call<Vacancy> editVacancy(@Path("VACANCY_ID") String VACANCY_ID, @Field("activity") String activity, @Field("time_interval") String time_interval,
                              @Field("salary") String salary, @Field("work_date") String work_date, @Field("workplaces") String workplaces,
                              @Field("address_go") String address_go, @Field("status") String status, @Field("title") String title,
                              @Field("content") String content);

    @GET("vacancy/")
    Call<List<Vacancy>> getVacancy(@Query("author") String id);


    @GET("admin/?getNonce=1")
    Call<String> getToken();

    @GET("users/me")
    Call<UserObject> testSet();

    @FormUrlEncoded
    @POST("auth")
    Call<UserObject> login(@Field("phone") String phone, @Field("password") String password);
}
