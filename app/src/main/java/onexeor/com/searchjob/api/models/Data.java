package onexeor.com.searchjob.api.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by OneXeor on 23.01.17
 * In 9:31.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Data implements Parcelable {

    private String ID;
    private String status;
    private Params params;
    private String user_id;
    private String phone;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Params getParams() {
        return params;
    }

    public void setParams(Params params) {
        this.params = params;
    }

    public Data() {
    }

    public String getID() {
        return ID;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getPhone() {
        return phone;
    }


    public Data(String status, Params params) {
        this.status = status;
        this.params = params;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Params implements Parcelable {
        String email;
        String username;
        String password;

        public Params(String email, String username, String password) {
            this.email = email;
            this.username = username;
            this.password = password;
        }

        public Params() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public String getEmail() {
            return email;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.email);
            dest.writeString(this.username);
            dest.writeString(this.password);
        }

        protected Params(Parcel in) {
            this.email = in.readString();
            this.username = in.readString();
            this.password = in.readString();
        }

        public final Parcelable.Creator<Params> CREATOR = new Parcelable.Creator<Params>() {
            @Override
            public Params createFromParcel(Parcel source) {
                return new Params(source);
            }

            @Override
            public Params[] newArray(int size) {
                return new Params[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.status);
        dest.writeParcelable(this.params, flags);
    }

    protected Data(Parcel in) {
        this.status = in.readString();
        this.params = in.readParcelable(Params.class.getClassLoader());
    }

    public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel source) {
            return new Data(source);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
}
