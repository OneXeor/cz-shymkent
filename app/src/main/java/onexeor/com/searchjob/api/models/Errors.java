package onexeor.com.searchjob.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by OneXeor on 25.01.17
 * In 10:25.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Errors {

    private String code;
    private String message;
    private String data;


    public Errors() {
    }

    public Errors(String code, String message, String data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
