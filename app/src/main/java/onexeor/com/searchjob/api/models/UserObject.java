package onexeor.com.searchjob.api.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by OneXeor on 23.01.17
 * In 10:39.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserObject  {
    @JsonProperty("ID")
    private String iD;
    @JsonProperty("user_login")
    private String userLogin;
    @JsonProperty("user_pass")
    private String userPass;
    @JsonProperty("user_nicename")
    private String userNicename;
    @JsonProperty("user_email")
    private String userEmail;
    @JsonProperty("user_url")
    private String userUrl;
    @JsonProperty("user_registered")
    private String userRegistered;
    @JsonProperty("user_activation_key")
    private String userActivationKey;
    @JsonProperty("user_status")
    private String userStatus;
    @JsonProperty("display_name")
    private String displayName;
    @JsonProperty("account_type")
    private String accountType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ID")
    public String getID() {
        return iD;
    }

    @JsonProperty("ID")
    public void setID(String iD) {
        this.iD = iD;
    }

    @JsonProperty("user_login")
    public String getUserLogin() {
        return userLogin;
    }

    @JsonProperty("user_login")
    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @JsonProperty("user_pass")
    public String getUserPass() {
        return userPass;
    }

    @JsonProperty("user_pass")
    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    @JsonProperty("user_nicename")
    public String getUserNicename() {
        return userNicename;
    }

    @JsonProperty("user_nicename")
    public void setUserNicename(String userNicename) {
        this.userNicename = userNicename;
    }

    @JsonProperty("user_email")
    public String getUserEmail() {
        return userEmail;
    }

    @JsonProperty("user_email")
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @JsonProperty("user_url")
    public String getUserUrl() {
        return userUrl;
    }

    @JsonProperty("user_url")
    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }

    @JsonProperty("user_registered")
    public String getUserRegistered() {
        return userRegistered;
    }

    @JsonProperty("user_registered")
    public void setUserRegistered(String userRegistered) {
        this.userRegistered = userRegistered;
    }

    @JsonProperty("user_activation_key")
    public String getUserActivationKey() {
        return userActivationKey;
    }

    @JsonProperty("user_activation_key")
    public void setUserActivationKey(String userActivationKey) {
        this.userActivationKey = userActivationKey;
    }

    @JsonProperty("user_status")
    public String getUserStatus() {
        return userStatus;
    }

    @JsonProperty("user_status")
    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    @JsonProperty("display_name")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("display_name")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @JsonProperty("account_type")
    public String getAccountType() {
        return accountType;
    }

    @JsonProperty("account_type")
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


}
