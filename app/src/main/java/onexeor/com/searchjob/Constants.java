package onexeor.com.searchjob;

/**
 * Created by OneXeor on 08.01.17
 * In 1:04 o'clock.
 */

public class Constants {
    public final static String PREFS_LANG = "language";
    public final static String PREFS_LOGIN = "login";
    public final static String PREFS_LOGIN_TYPE_USER = "login_type";
    public final static String PREFS_PHONE_NUMBER = "phone";
    public final static String PREFS_PASSWORD = "password";
    public final static String PREFS_USER_ID = "user_id";
    public final static String PREFS_IS_LOGGED = "is_logged";


    public final static String PREFS_NONCE = "Nonce";

}
