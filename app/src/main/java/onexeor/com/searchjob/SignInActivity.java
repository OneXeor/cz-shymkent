package onexeor.com.searchjob;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;

import onexeor.com.searchjob.fragments.SignInFragments.FragmentSignIn;

public class SignInActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        if (Helper.getSPreferenceLogin(this).getBoolean(Constants.PREFS_IS_LOGGED, false)) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
        }

        Helper.jumpFragment(R.id.signIn_container, getSupportFragmentManager(), FragmentSignIn.newInstance(), false).commit();
        func(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        func(toolbar);
    }

    //listener фрагментов для смены бургер и home button
    private void func(final Toolbar toolbar) {
        if (toolbar != null) {
            getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        if (getSupportActionBar() != null)
                            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onBackPressed();
                            }
                        });
                    } else {
                        usualFragments();
                    }
                }
            });
        }
    }

    private void usualFragments() {
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            onCreateDialogExit().show();
        } else {
            super.onBackPressed();
        }
    }

    //Диалог выхода из приложения
    public Dialog onCreateDialogExit() {
        return new MaterialDialog.Builder(this)
                .title(R.string.exit)
                .positiveText(R.string.yes)
                .content(R.string.you_sure_want_exit)
                .theme(Theme.LIGHT)
                .negativeText(R.string.no)
                .negativeColor(Color.parseColor("#8b000000"))
                .positiveColor(getResources().getColor(R.color.colorAccent))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                }).build();
    }
}
