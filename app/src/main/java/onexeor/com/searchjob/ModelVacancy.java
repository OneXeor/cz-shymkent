package onexeor.com.searchjob;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by OneXeor on 16.01.17
 * In 14:10.
 */

public class ModelVacancy implements Parcelable {

    private String title;
    private String date;
    private String price;
    private String address;
    private String title_agency;
    private String desc_vacancy;
    private String phone;

    public ModelVacancy(String title, String date, String price, String address, String title_agency, String desc_vacancy, String phone) {
        this.title = title;
        this.date = date;
        this.price = price;
        this.address = address;
        this.title_agency = title_agency;
        this.desc_vacancy = desc_vacancy;
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDesc_vacancy() {
        return desc_vacancy;
    }

    public void setDesc_vacancy(String desc_vacancy) {
        this.desc_vacancy = desc_vacancy;
    }

    public String getTitle_agency() {
        return title_agency;
    }

    public void setTitle_agency(String title_agency) {
        this.title_agency = title_agency;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.date);
        dest.writeString(this.price);
        dest.writeString(this.address);
        dest.writeString(this.title_agency);
        dest.writeString(this.desc_vacancy);
        dest.writeString(this.phone);
    }

    protected ModelVacancy(Parcel in) {
        this.title = in.readString();
        this.date = in.readString();
        this.price = in.readString();
        this.address = in.readString();
        this.title_agency = in.readString();
        this.desc_vacancy = in.readString();
        this.phone = in.readString();
    }

    public static final Creator<ModelVacancy> CREATOR = new Creator<ModelVacancy>() {
        @Override
        public ModelVacancy createFromParcel(Parcel source) {
            return new ModelVacancy(source);
        }

        @Override
        public ModelVacancy[] newArray(int size) {
            return new ModelVacancy[size];
        }
    };
}
