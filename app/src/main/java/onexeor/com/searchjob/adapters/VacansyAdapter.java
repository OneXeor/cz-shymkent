package onexeor.com.searchjob.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.List;

import onexeor.com.searchjob.Helper;
import onexeor.com.searchjob.MainActivity;
import onexeor.com.searchjob.ModelVacancy;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.api.models.Vacancy;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.FragmentDescVacancy;

/**
 * Created by OneXeor on 16.01.17
 * In 14:01.
 */

public class VacansyAdapter extends RecyclerView.Adapter<VacansyAdapter.VacancyHolder> {

    private List<Vacancy> arr_vac;
    private Context context;

    public VacansyAdapter(Context context, List<Vacancy> arr_vac) {
        this.arr_vac = arr_vac;
        this.context = context;
    }

    @Override
    public VacancyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vacancy, null);
        return new VacancyHolder(v);
    }

    @Override
    public void onBindViewHolder(VacancyHolder holder, int position) {
        final Vacancy item = arr_vac.get(position);
        holder.tv_date_vac.setText(item.getDate());
        holder.tv_title_vac.setText(item.getTitle().getRendered());
        holder.tv_price_vac.setText(item.getSalary());
        holder.mrl_vac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.jumpFragment(R.id.container, ((MainActivity) context).getSupportFragmentManager(), FragmentDescVacancy.newInstance(item), false).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arr_vac.size();
    }

    public class VacancyHolder extends RecyclerView.ViewHolder {

        private TextView tv_title_vac, tv_price_vac, tv_date_vac;
        private MaterialRippleLayout mrl_vac;

        public VacancyHolder(View itemView) {
            super(itemView);

            mrl_vac = (MaterialRippleLayout) itemView.findViewById(R.id.mrl_vac);
            tv_title_vac = (TextView) itemView.findViewById(R.id.tv_title_vac);
            tv_price_vac = (TextView) itemView.findViewById(R.id.tv_count_money);
            tv_date_vac = (TextView) itemView.findViewById(R.id.tv_date_vac);
        }

    }

}
