package onexeor.com.searchjob;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by onexeor on 07.01.17.
 */

public class Helper {

    /**
     * Has animation TRANSIT_FRAGMENT_FADE.
     * @return Returns the same {@link android.app.FragmentTransaction} instance.
     * */
    public static FragmentTransaction jumpFragment(int containerView, FragmentManager fm, Fragment fragment, boolean isAdd) {
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        if (isAdd) {
            fragmentTransaction.add(containerView, fragment, null);
        } else {
            fragmentTransaction.replace(containerView, fragment, null);
        }
        return fragmentTransaction;
    }

    public static SharedPreferences getSPreferenceLogin(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.PREFS_LOGIN, Context.MODE_PRIVATE);
        return sharedPreferences;
    }
}
