package onexeor.com.searchjob;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by OneXeor on 17.01.17
 * In 2:15.
 */

public class NewsAgencyModel implements Parcelable {

    private String title;
    private String date;
    private String desc;

    public NewsAgencyModel(String title, String date, String desc) {
        this.title = title;
        this.date = date;
        this.desc = desc;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.date);
        dest.writeString(this.desc);
    }

    protected NewsAgencyModel(Parcel in) {
        this.title = in.readString();
        this.date = in.readString();
        this.desc = in.readString();
    }

    public static final Parcelable.Creator<NewsAgencyModel> CREATOR = new Parcelable.Creator<NewsAgencyModel>() {
        @Override
        public NewsAgencyModel createFromParcel(Parcel source) {
            return new NewsAgencyModel(source);
        }

        @Override
        public NewsAgencyModel[] newArray(int size) {
            return new NewsAgencyModel[size];
        }
    };
}
