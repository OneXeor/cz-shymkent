package onexeor.com.searchjob.fragments.SignInFragments.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import onexeor.com.searchjob.Constants;
import onexeor.com.searchjob.Helper;
import onexeor.com.searchjob.MainActivity;
import onexeor.com.searchjob.ModelVacancy;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.api.Scheme;
import onexeor.com.searchjob.api.ServiceGenerator;
import onexeor.com.searchjob.api.models.RegObject;
import onexeor.com.searchjob.api.models.Vacancy;
import onexeor.com.searchjob.fragments.BaseFragment;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments.FragmentAddVacancy;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by OneXeor on 17.01.17
 * In 0:26.
 */

public class FragmentDescVacancy extends BaseFragment implements View.OnClickListener {

    private TextView tv_name_agency;
    private TextView tv_title_vac;
    private TextView tv_coast_vac;
    private TextView tv_address_vac;
    private TextView tv_desc_vac;
    private Button btn_response_vac;
    private Vacancy model;
    private FloatingActionButton fab_edit;


    public static FragmentDescVacancy newInstance(Vacancy item) {
        FragmentDescVacancy descVacancy = new FragmentDescVacancy();
        Bundle bundle = new Bundle();
        bundle.putParcelable("item", item);
        descVacancy.setArguments(bundle);
        return descVacancy;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_desc_vacancy, container, false);
        tv_address_vac = (TextView) rootView.findViewById(R.id.tv_address_vac);
        fab_edit = (FloatingActionButton) rootView.findViewById(R.id.fab_edit);
        tv_coast_vac = (TextView) rootView.findViewById(R.id.tv_coast_money_vac);
        tv_title_vac = (TextView) rootView.findViewById(R.id.tv_name_vacancy);
        tv_name_agency = (TextView) rootView.findViewById(R.id.tv_name_agency);
        tv_desc_vac = (TextView) rootView.findViewById(R.id.tv_desc_vacancy);
        btn_response_vac = (Button) rootView.findViewById(R.id.btn_response_vac);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Вакансия");
        }

        Bundle bundle = getArguments();
        if (bundle != null) {
            model = bundle.getParcelable("item");
            tv_desc_vac.setText(model.getContent().getRendered());
            tv_address_vac.setText(model.getAddress_go());
            tv_name_agency.setText(model.getTitle().getRendered());
            tv_coast_vac.setText(model.getSalary());
            tv_title_vac.setText(model.getTitle().getRendered());
        }
        fab_edit.setVisibility(View.GONE);

        if (Helper.getSPreferenceLogin(getActivity()).getInt(Constants.PREFS_LOGIN_TYPE_USER, 0) == 0) {
            btn_response_vac.setText(getActivity().getString(R.string.vacancy));
        } else {
            btn_response_vac.setText("Удалить вакансию");
            fab_edit.setOnClickListener(this);
        }

        if (Helper.getSPreferenceLogin(getActivity()).getInt(Constants.PREFS_LOGIN_TYPE_USER, 0) == 0) {
            fab_edit.setVisibility(View.GONE);

        } else {
            fab_edit.setVisibility(View.VISIBLE);
        }
        btn_response_vac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Helper.getSPreferenceLogin(getActivity()).getInt(Constants.PREFS_LOGIN_TYPE_USER, 0) == 0) {

                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_response_vac, null);
                    Button btn_make_a_call = (Button) view.findViewById(R.id.btn_make_a_call);
                    MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                            .customView(view, true)
                            .show();
                    btn_make_a_call.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (getActivity() != null)
                                Toast.makeText(getActivity(), model.getPhone(), Toast.LENGTH_SHORT).show();
                            String uri = "tel:" + model.getPhone().trim();
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse(uri));
                            startActivity(intent);
                        }
                    });
                } else {

                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_delete_vac, null);
                    Button btn_yes = (Button) view.findViewById(R.id.btn_yes);
                    Button btn_no = (Button) view.findViewById(R.id.btn_no);
                    final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                            .customView(view, true)
                            .show();
                    btn_yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteVac();
                            dialog.dismiss();
                        }
                    });
                    btn_no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            dialog.dismiss();
                        }
                    });

                }
            }
        });
    }

    private void deleteVac() {
        String str = Helper.getSPreferenceLogin(getActivity()).getString(Constants.PREFS_PHONE_NUMBER, "").replace("+", "");
        Scheme scheme = ServiceGenerator.createServices(Scheme.class, str, Helper.getSPreferenceLogin(getActivity()).getString(Constants.PREFS_PASSWORD, ""));
        Call<RegObject> call = scheme.deleteVacancy(model.getId());
        call.enqueue(new Callback<RegObject>() {
            @Override
            public void onResponse(Call<RegObject> call, Response<RegObject> response) {
                if (response.body() != null) {
                    Log.e("response", response.body().getMessage() + "");
                    getActivity().onBackPressed();
                }

                if (response.errorBody() != null) {
                    Gson gson = new GsonBuilder().create();
                    try {
                        RegObject mApiError = gson.fromJson(response.errorBody().string(), RegObject.class);
                        if (mApiError != null) {
                            if (getActivity() != null)
                                Toast.makeText(getActivity(), mApiError.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RegObject> call, Throwable t) {
                Log.e("ResponseErr", t.toString());

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_edit:
                if (model != null)
                    Helper.jumpFragment(R.id.container, getActivity().getSupportFragmentManager(), FragmentAddVacancy.newInstance(model), false).addToBackStack(null).commit();
                break;
        }
    }
}
