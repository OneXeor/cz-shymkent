package onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import onexeor.com.searchjob.Constants;
import onexeor.com.searchjob.MainActivity;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.SignInActivity;
import onexeor.com.searchjob.api.Scheme;
import onexeor.com.searchjob.api.ServiceGenerator;
import onexeor.com.searchjob.api.models.Errors;
import onexeor.com.searchjob.api.models.RegObject;
import onexeor.com.searchjob.fragments.BaseFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by OneXeor on 25.01.17
 * In 3:24.
 */

public class FragmentAnketaEmployer extends BaseFragment {


    private View rootView;
    private Call<RegObject> objectCall;
    private TextInputEditText edt_code;
    private Button btn_next;
    private SharedPreferences sp_login;
    private SharedPreferences.Editor sp_login_edit;
    private TextView tv_face_user;
    private EditText edt_name, edt_iin, edt_deiatel, edt_phone;
    private boolean legal_status = false;
    private boolean isEdit = false;
    private RegObject item;

    public static FragmentAnketaEmployer newInstance(RegObject item, boolean isEdit) {
        FragmentAnketaEmployer fragmentAnketa = new FragmentAnketaEmployer();
        Bundle bundle = new Bundle();
        bundle.putParcelable("anketa", item);
        bundle.putBoolean("isEdit", isEdit);
        fragmentAnketa.setArguments(bundle);
        return fragmentAnketa;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_anketa_employer, container, false);
        edt_phone = (EditText) rootView.findViewById(R.id.edt_phone);
        edt_deiatel = (EditText) rootView.findViewById(R.id.edt_deiatel);
        edt_iin = (EditText) rootView.findViewById(R.id.edt_iin);
        edt_name = (EditText) rootView.findViewById(R.id.edt_name);
        btn_next = (Button) rootView.findViewById(R.id.btn_search_worck);
        tv_face_user = (TextView) rootView.findViewById(R.id.tv_face_user);
        sp_login = getActivity().getSharedPreferences(Constants.PREFS_LOGIN, Context.MODE_PRIVATE);
        sp_login_edit = sp_login.edit();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Заполните данные");
        }
        Bundle bundle = getArguments();
        isEdit = bundle.getBoolean("isEdit");
        item = bundle.getParcelable("anketa");

        if (item != null) {
            edt_phone.setText(item.getPhone());
            edt_deiatel.setText(item.getActivity());
            edt_iin.setText(item.getInn());
            edt_name.setText(item.getCompany_name());
            tv_face_user.setText((Boolean.parseBoolean(item.getLegal_status()) ? "Юридическое лицо" : "Физическое лицо"));
            legal_status = Boolean.parseBoolean(item.getLegal_status());
        }
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_name.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Введите название компании", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_iin.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Введите ИИН компании", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_deiatel.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Введите вид деятельноести", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (edt_phone.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Введите номер телефона", Toast.LENGTH_SHORT).show();
                    return;
                }
                sendAnketa();
            }
        });

        tv_face_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getActivity())
                        .items((CharSequence[]) new String[]{"Физическое лицо", "Юридическое лицо"})
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                                switch (position) {
                                    case 0:
                                        tv_face_user.setText(text.toString());
                                        legal_status = false;
                                        break;
                                    case 1:
                                        tv_face_user.setText(text.toString());
                                        legal_status = true;
                                        break;
                                }
                            }
                        })
                        .show();
            }
        });
    }

    private void sendAnketa() {
        String acc_type;
        if (sp_login.getInt(Constants.PREFS_LOGIN_TYPE_USER, 0) == 0) {
            acc_type = "Соискатель";
        } else {
            acc_type = "Работодатель";
        }
        Scheme scheme = ServiceGenerator.createService(Scheme.class, true);
        objectCall = scheme.edit_anket(sp_login.getString(Constants.PREFS_USER_ID, ""), acc_type, sp_login.getString(Constants.PREFS_LOGIN, ""), "", edt_iin.getText().toString(),
                edt_deiatel.getText().toString(), legal_status, edt_name.getText().toString(), "");

        objectCall.enqueue(new Callback<RegObject>() {
            @Override
            public void onResponse(Call<RegObject> call, Response<RegObject> response) {
                if (response.body() != null) {
                    Log.e("Response", response.body().getName());
                    if (sp_login_edit.putBoolean(Constants.PREFS_IS_LOGGED, true).commit()) {
                        if (isEdit) {
                            getActivity().recreate();
                            return;
                        }
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        getActivity().startActivity(intent);
                        Bundle bundle = getArguments();
                        isEdit = bundle.getBoolean("isEdit");

                        getActivity().finish();
                    }

                } else if (response.errorBody() != null) {
                    Gson gson = new GsonBuilder().create();
                    try {
                        RegObject mApiError = gson.fromJson(response.errorBody().string(), RegObject.class);
                        if (mApiError != null) {
                            Log.e("error body", mApiError.getMessage() + "");
                            if (getActivity() != null)
                                Toast.makeText(getActivity(), mApiError.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RegObject> call, Throwable t) {
                Log.e("error body", t.toString());
            }
        });
    }
}
