package onexeor.com.searchjob.fragments.SignInFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import onexeor.com.searchjob.Constants;
import onexeor.com.searchjob.Helper;
import onexeor.com.searchjob.MainActivity;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.SignInActivity;
import onexeor.com.searchjob.api.Scheme;
import onexeor.com.searchjob.api.ServiceGenerator;
import onexeor.com.searchjob.api.models.RegObject;
import onexeor.com.searchjob.fragments.BaseFragment;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.applicantFragments.FragmentAnketaApplicant;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments.FragmentAnketaEmployer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by OneXeor on 08.01.17
 * In 1:18 o'clock.
 */

public class FragmentInputPass extends BaseFragment implements View.OnClickListener {

    private View rootView;
    private TextInputEditText edt_pass, edt_pass_repeat;
    private Button btn_next;
    private Call<RegObject> objectCall;

    public static FragmentInputPass newInstance() {
        return new FragmentInputPass();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_input_password, container, false);
        btn_next = (Button) rootView.findViewById(R.id.btn_next);
        edt_pass = (TextInputEditText) rootView.findViewById(R.id.edt_pass);
        edt_pass_repeat = (TextInputEditText) rootView.findViewById(R.id.edt_pass_repeat);
        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        ActionBar actionBar = ((SignInActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Регистрация");
        }
        btn_next.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                if (edt_pass.getText().toString().equals(edt_pass_repeat.getText().toString())) {
                    inputPass();
                } else {
                    Toast.makeText(getActivity(), "Пароли не совпадают", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void inputPass() {
        Scheme scheme = ServiceGenerator.createService(Scheme.class, false);
        objectCall = scheme.register_three(Helper.getSPreferenceLogin(getActivity()).getString(Constants.PREFS_PHONE_NUMBER, "").replace("+", "").replaceAll("\\s+", "").replaceAll("-", ""), edt_pass.toString());
        Log.e("response", objectCall.request().url() + " ");

        objectCall.enqueue(new Callback<RegObject>() {
            @Override
            public void onResponse(Call<RegObject> call, Response<RegObject> response) {
                if (response.body() != null) {
                    Log.e("response", response.body().getData().getUser_id());
                    if (Helper.getSPreferenceLogin(getActivity()).edit().putString(Constants.PREFS_USER_ID, response.body().getData().getUser_id()).commit()) {
                        if (Helper.getSPreferenceLogin(getActivity()).getInt(Constants.PREFS_LOGIN_TYPE_USER, 0) == 0)
                            Helper.jumpFragment(R.id.signIn_container, getActivity().getSupportFragmentManager(), FragmentAnketaApplicant.newInstance(false, null), false).addToBackStack(null).commit();
                        else
                            Helper.jumpFragment(R.id.signIn_container, getActivity().getSupportFragmentManager(), FragmentAnketaEmployer.newInstance(null, false), false).addToBackStack(null).commit();
                    }
                }
            }

            @Override
            public void onFailure(Call<RegObject> call, Throwable t) {
                Log.e("errorInput", t.toString());
            }
        });
    }
}
