package onexeor.com.searchjob.fragments.SignInFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import onexeor.com.searchjob.Constants;
import onexeor.com.searchjob.Helper;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.SignInActivity;
import onexeor.com.searchjob.api.Scheme;
import onexeor.com.searchjob.api.ServiceGenerator;
import onexeor.com.searchjob.api.models.RegObject;
import onexeor.com.searchjob.fragments.BaseFragment;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.applicantFragments.FragmentAnketaApplicant;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments.FragmentAnketaEmployer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by OneXeor on 08.01.17
 * In 1:18 o'clock.
 */

public class FragmentInputSMS extends BaseFragment {

    private View rootView;
    private Call<RegObject> objectCall;
    private TextInputEditText edt_code;
    private Button btn_next;

    public static FragmentInputSMS newInstance(String user_id) {
        FragmentInputSMS fragmentInputSMS = new FragmentInputSMS();
        Bundle bundle = new Bundle();
        bundle.putString("user_id", user_id);
        fragmentInputSMS.setArguments(bundle);
        return fragmentInputSMS;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_input_sms, container, false);
        edt_code = (TextInputEditText) rootView.findViewById(R.id.edt_code);
        btn_next = (Button) rootView.findViewById(R.id.btn_next);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionBar = ((SignInActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Регистрация");
        }

        Bundle bundle = getArguments();
        final String user_id = bundle.getString("user_id");


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrationTwo(user_id);

            }
        });
    }


    private void registrationTwo(String user_id) {
        if (objectCall != null)
            objectCall.cancel();
        Scheme scheme = ServiceGenerator.createService(Scheme.class);
        objectCall = scheme.register_two(user_id, edt_code.getText().toString());
        Log.e("response", edt_code.getText().toString());

        if (edt_code.getText().toString().contains("1111")) {
            Helper.jumpFragment(R.id.signIn_container, getActivity().getSupportFragmentManager(), FragmentInputPass.newInstance(), false).addToBackStack(null).commit();
        } else {
            objectCall.enqueue(new Callback<RegObject>() {
                @Override
                public void onResponse(Call<RegObject> call, Response<RegObject> response) {
                    if (response.body() != null) {
                        if (response.body().getMessage() == null) {
                            Helper.jumpFragment(R.id.signIn_container, getActivity().getSupportFragmentManager(), FragmentInputPass.newInstance(), false).addToBackStack(null).commit();
                            Log.e("response", response.body().getResult() + "\n" + response.body().getData().getUser_id());
                        } else {
                            edt_code.setError(response.body().getMessage());
                            Log.e("response", response.body().getMessage() + " mess");
                        }
                    } else {
                        if (response.errorBody() != null) {
                            Gson gson = new GsonBuilder().create();
                            try {
                                RegObject mApiError = gson.fromJson(response.errorBody().string(), RegObject.class);
                                if (mApiError != null)
                                    Toast.makeText(getActivity(), mApiError.getMessage(), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(getActivity(), "Не верно введён код", Toast.LENGTH_SHORT).show();

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.e("response", " null");
                        }
                    }
                }

                @Override
                public void onFailure(Call<RegObject> call, Throwable t) {
                    Log.e("errorRegister", t.toString());
                    if (t.toString().contains("No address associated with hostname")) {
                        Toast.makeText(getActivity(), "Нет соединения с сервером", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
