package onexeor.com.searchjob.fragments.SignInFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import onexeor.com.searchjob.Constants;
import onexeor.com.searchjob.Helper;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.SignInActivity;
import onexeor.com.searchjob.fragments.BaseFragment;

/**
 * Created by OneXeor on 08.01.17
 * In 1:18 o'clock.
 */

public class FragmentChRegMode extends BaseFragment implements View.OnClickListener {

    private Button btn_emplouer, btn_applicant;

    public static FragmentChRegMode newInstance() {
        return new FragmentChRegMode();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_mode_reg, container, false);
        btn_applicant = (Button) rootView.findViewById(R.id.btn_applicant);
        btn_emplouer = (Button) rootView.findViewById(R.id.btn_employer);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionBar = ((SignInActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Режим регистрации");
        }

        btn_applicant.setOnClickListener(this);
        btn_emplouer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_applicant:
                if (Helper.getSPreferenceLogin(getActivity()).edit().putInt(Constants.PREFS_LOGIN_TYPE_USER, 0).commit())
                    Helper.jumpFragment(R.id.signIn_container, getActivity().getSupportFragmentManager(), FragmentRegistration.newInstance(), false).addToBackStack(null).commit();
                break;
            case R.id.btn_employer:
                if (Helper.getSPreferenceLogin(getActivity()).edit().putInt(Constants.PREFS_LOGIN_TYPE_USER, 1).commit())
                    Helper.jumpFragment(R.id.signIn_container, getActivity().getSupportFragmentManager(), FragmentRegistration.newInstance(), false).addToBackStack(null).commit();
                break;
        }
    }
}
