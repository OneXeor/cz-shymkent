package onexeor.com.searchjob.fragments.SignInFragments.fragments.applicantFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import onexeor.com.searchjob.MainActivity;
import onexeor.com.searchjob.ModelVacancy;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.adapters.VacansyAdapter;
import onexeor.com.searchjob.api.Scheme;
import onexeor.com.searchjob.api.ServiceGenerator;
import onexeor.com.searchjob.api.models.Vacancy;
import onexeor.com.searchjob.fragments.BaseFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by OneXeor on 08.01.17
 * In 1:18 o'clock.
 */

public class FragmentVacansyListOfApplicant extends BaseFragment implements View.OnClickListener {


    private RecyclerView recyclerView;
    private Call<List<Vacancy>> listCall;

    public static FragmentVacansyListOfApplicant newInstance() {
        return new FragmentVacansyListOfApplicant();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_vacansy_of_applicant, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Поиск работы");
        }


        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);


        getVacancies();

    }


    private void getVacancies() {
        if (listCall != null)
            listCall.cancel();

        Scheme scheme = ServiceGenerator.createService(Scheme.class, true);
        listCall = scheme.getVacancies();
        listCall.enqueue(new Callback<List<Vacancy>>() {
            @Override
            public void onResponse(Call<List<Vacancy>> call, Response<List<Vacancy>> response) {
                if (response.errorBody() != null)
                    try {
                        Log.e("response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                else if (response.body() != null) {
                    VacansyAdapter vacansy = new VacansyAdapter(getActivity(), response.body());
                    recyclerView.setAdapter(vacansy);
                } else {
                    Log.e("response", "response is null");

                }
            }

            @Override
            public void onFailure(Call<List<Vacancy>> call, Throwable t) {
                Log.e("error", t.toString());
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }
}
