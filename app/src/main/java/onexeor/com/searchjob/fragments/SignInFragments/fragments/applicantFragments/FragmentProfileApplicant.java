package onexeor.com.searchjob.fragments.SignInFragments.fragments.applicantFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import onexeor.com.searchjob.Constants;
import onexeor.com.searchjob.Helper;
import onexeor.com.searchjob.MainActivity;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.api.Scheme;
import onexeor.com.searchjob.api.ServiceGenerator;
import onexeor.com.searchjob.api.models.RegObject;
import onexeor.com.searchjob.fragments.BaseFragment;
import onexeor.com.searchjob.fragments.SignInFragments.EmployerViewPagerAdapter;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments.FragmentAnketaEmployer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by OneXeor on 08.01.17
 * In 1:18 o'clock.
 */

public class FragmentProfileApplicant extends BaseFragment implements View.OnClickListener {


    private TextView tv_username, tv_age, tv_address_vac, tv_about_u;
    private Button btn_change_profile;
    private LinearLayout ll_profile;
    private RegObject applicant;

    public static FragmentProfileApplicant newInstance() {
        return new FragmentProfileApplicant();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile_applicant, container, false);
        tv_address_vac = (TextView) rootView.findViewById(R.id.tv_address_vac);
        tv_username = (TextView) rootView.findViewById(R.id.tv_username);
        tv_age = (TextView) rootView.findViewById(R.id.tv_age);
        tv_about_u = (TextView) rootView.findViewById(R.id.tv_about_u);
        ll_profile = (LinearLayout) rootView.findViewById(R.id.ll_profile);
        btn_change_profile = (Button) rootView.findViewById(R.id.btn_change_profile);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Профиль");
        }

        ll_profile.setVisibility(View.GONE);
        getUserdata();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_change_profile:
                Helper.jumpFragment(R.id.container, getActivity().getSupportFragmentManager(), FragmentAnketaApplicant.newInstance(true, applicant), false).addToBackStack(null).commit();
                break;
        }
    }


    private void getUserdata() {
        String str_phone = Helper.getSPreferenceLogin(getActivity()).getString(Constants.PREFS_PHONE_NUMBER, "").replace("+", "");
        String str_pass = Helper.getSPreferenceLogin(getActivity()).getString(Constants.PREFS_PASSWORD, "");
        String str_id = Helper.getSPreferenceLogin(getActivity()).getString(Constants.PREFS_USER_ID, "");

        Scheme scheme = ServiceGenerator.createServices(Scheme.class, str_phone, str_pass);
        Call<RegObject> call = scheme.getAnket(str_id);
        Log.e("ListArgs", call.request().url() + "\n" +
                str_id + " " + str_pass + "\n" + str_phone);
        call.enqueue(new Callback<RegObject>() {
            @Override
            public void onResponse(Call<RegObject> call, Response<RegObject> response) {
                if (response.body() != null) {
                    Log.e("response", response.body().getCompany_name() + "\n" + response.body().getAge());
                    applicant = response.body();
                    btn_change_profile.setOnClickListener(FragmentProfileApplicant.this);
                    tv_about_u.setText((response.body().getDescription() != null ? response.body().getDescription() : ""));
                    tv_username.setText(response.body().getCompany_name());
                    tv_age.setText(response.body().getAge());
                    tv_address_vac.setText("нету");
                    ll_profile.setVisibility(View.VISIBLE);
                }
                if (response.errorBody() != null) {
                    Gson gson = new GsonBuilder().create();
                    try {
                        RegObject mApiError = gson.fromJson(response.errorBody().string(), RegObject.class);
                        if (mApiError != null)
                            Log.e("Response", mApiError.getMessage() + " ");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RegObject> call, Throwable t) {
                Log.e("response", t.toString());
            }
        });
    }
}
