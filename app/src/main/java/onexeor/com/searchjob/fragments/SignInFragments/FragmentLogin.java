package onexeor.com.searchjob.fragments.SignInFragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.ResponseBody;
import onexeor.com.searchjob.Constants;
import onexeor.com.searchjob.Helper;
import onexeor.com.searchjob.MainActivity;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.SignInActivity;
import onexeor.com.searchjob.api.Scheme;
import onexeor.com.searchjob.api.ServiceGenerator;
import onexeor.com.searchjob.api.models.RegObject;
import onexeor.com.searchjob.api.models.UserObject;
import onexeor.com.searchjob.fragments.BaseFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by OneXeor on 08.01.17
 * In 1:18 o'clock.
 */

public class FragmentLogin extends BaseFragment implements View.OnClickListener {

    private View rootView;
    private TextInputEditText edt_phone, edt_password;
    private Button btn_next;

    public static FragmentLogin newInstance() {
        return new FragmentLogin();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_login, container, false);
        btn_next = (Button) rootView.findViewById(R.id.btn_next);
        edt_phone = (TextInputEditText) rootView.findViewById(R.id.edt_phone);
        edt_password = (TextInputEditText) rootView.findViewById(R.id.edt_pass);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionBar = ((SignInActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Вход в личный кабинет");
        }

        edt_phone.setText("+7");
        edt_phone.addTextChangedListener(new TextWatcher() {
            int length_before = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                length_before = s.length();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (length_before < s.length()) {
                    if (s.length() == 10 || s.length() == 13)
                        s.append("-");
                    if (s.length() == 2 && s.length() == 5)
                        s.append(" ");

                    if (s.length() > 2) {
                        if (Character.isDigit(s.charAt(2)))
                            s.insert(2, " ");
                    }
                    if (s.length() > 6) {
                        if (Character.isDigit(s.charAt(6)))
                            s.insert(6, " ");
                    }
                    if (s.length() > 10) {
                        if (Character.isDigit(s.charAt(10)))
                            s.insert(10, "-");
                    }
                    if (s.length() > 13) {
                        if (Character.isDigit(s.charAt(13)))
                            s.insert(13, "-");
                    }
                }
                if (!s.toString().startsWith("+7")) {
                    edt_phone.setText("+7");
                    Selection.setSelection(edt_phone.getText(), edt_phone
                            .getText().length());

                }
            }
        });
        btn_next.setOnClickListener(this);
    }


    private void login() {
        Scheme scheme = ServiceGenerator.createService(Scheme.class, false);
        Call<UserObject> call = scheme.login(edt_phone.getText().toString().replace("+", "").replaceAll("-", "").replaceAll("\\s+", ""), edt_password.getText().toString());
        Log.e("Response", call.request().url() + " " + edt_phone.getText().toString().replace("+", "").replaceAll("-", "").replaceAll("\\s+", "") + "\n" + edt_password.getText().toString());

        call.enqueue(new Callback<UserObject>() {
            @Override
            public void onResponse(Call<UserObject> call, Response<UserObject> response) {
                if (response.body() != null) {
                    //                        Log.e("response", response.body().string());
//                        String responseBody = response.body().string();
//                        Gson gson = new GsonBuilder().create();
                    UserObject mApiError = response.body();
                    SharedPreferences.Editor sp_login_edit = Helper.getSPreferenceLogin(getActivity()).edit();
                    sp_login_edit.putString(Constants.PREFS_PASSWORD, edt_password.getText().toString());
                    sp_login_edit.putString(Constants.PREFS_PHONE_NUMBER, edt_phone.getText().toString());
                    sp_login_edit.putString(Constants.PREFS_USER_ID, String.valueOf(mApiError.getID()));
                    sp_login_edit.putInt(Constants.PREFS_LOGIN_TYPE_USER, (mApiError.getAccountType() != null && mApiError.getAccountType().toLowerCase().contains("раб".toLowerCase()) ? 1 : 0));
                    sp_login_edit.putBoolean(Constants.PREFS_IS_LOGGED, true);

                    if (sp_login_edit.commit()) {
                        Log.e("Response", mApiError.getDisplayName() + "\n" + mApiError.getAccountType() + "\n"
                                + mApiError.getID());
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                    }
                }
                if (response.errorBody() != null) {
                    Gson gson = new GsonBuilder().create();
                    try {
                        RegObject mApiError = gson.fromJson(response.errorBody().string(), RegObject.class);
                        if (mApiError != null) {
                            Log.e("Response", mApiError.getMessage() + " ");
                            edt_phone.setError(mApiError.getMessage());
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserObject> call, Throwable t) {
                Log.e("ResponseError", t.toString());

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                String phone = edt_phone.getText().toString();
                if (phone.isEmpty()) {
                    return;
                }
                login();
                break;
        }
    }
}
