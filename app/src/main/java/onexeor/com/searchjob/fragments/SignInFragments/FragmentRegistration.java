package onexeor.com.searchjob.fragments.SignInFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import onexeor.com.searchjob.Constants;
import onexeor.com.searchjob.Helper;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.SignInActivity;
import onexeor.com.searchjob.api.Scheme;
import onexeor.com.searchjob.api.ServiceGenerator;
import onexeor.com.searchjob.api.models.Errors;
import onexeor.com.searchjob.api.models.RegObject;
import onexeor.com.searchjob.fragments.BaseFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by OneXeor on 08.01.17
 * In 1:18 o'clock.
 */

public class FragmentRegistration extends BaseFragment implements View.OnClickListener {

    private TextInputEditText edt_phone;
    private CheckBox chb_agreenment;
    private Button btn_next;
    private Call<RegObject> objectCall;


    public static FragmentRegistration newInstance() {
        return new FragmentRegistration();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reg, container, false);
        edt_phone = (TextInputEditText) rootView.findViewById(R.id.edt_code);
        chb_agreenment = (CheckBox) rootView.findViewById(R.id.chb_agreenment);
        btn_next = (Button) rootView.findViewById(R.id.btn_next);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionBar = ((SignInActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Регистрация");
        }

        edt_phone.setText("+7");
        edt_phone.addTextChangedListener(new TextWatcher() {
            int length_before = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                length_before = s.length();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (length_before < s.length()) {
                    if (s.length() == 10 || s.length() == 13)
                        s.append("-");
                    if (s.length() == 2 && s.length() == 5)
                        s.append(" ");

                    if (s.length() > 2) {
                        if (Character.isDigit(s.charAt(2)))
                            s.insert(2, " ");
                    }
                    if (s.length() > 6) {
                        if (Character.isDigit(s.charAt(6)))
                            s.insert(6, " ");
                    }
                    if (s.length() > 10) {
                        if (Character.isDigit(s.charAt(10)))
                            s.insert(10, "-");
                    }
                    if (s.length() > 13) {
                        if (Character.isDigit(s.charAt(13)))
                            s.insert(13, "-");
                    }
                }
                if (!s.toString().startsWith("+7")) {
                    edt_phone.setText("+7");
                    Selection.setSelection(edt_phone.getText(), edt_phone
                            .getText().length());

                }
            }
        });

        btn_next.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                if (edt_phone.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Вы не ввели номер телефона", Toast.LENGTH_SHORT).show();
                } else {
                    if (edt_phone.getText().toString().length() < 11) {
                        Toast.makeText(getActivity(), "Вы ввели не верный номер телефона", Toast.LENGTH_SHORT).show();
                    } else {
                        if (chb_agreenment.isChecked()) {
                            if (Helper.getSPreferenceLogin(getActivity()).edit().putString(Constants.PREFS_PHONE_NUMBER, edt_phone.getText().toString()).commit()) {
                                registration(edt_phone.getText().toString());
                            }
                        } else {
                            Toast.makeText(getActivity(), "Вы не приняли условия соглашения", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }


    private void registration(String usernae) {
        if (objectCall != null)
            objectCall.cancel();
        Scheme scheme = ServiceGenerator.createService(Scheme.class);
        objectCall = scheme.register(usernae);
        objectCall.enqueue(new Callback<RegObject>() {
            @Override
            public void onResponse(Call<RegObject> call, Response<RegObject> response) {
                if (Helper.getSPreferenceLogin(getContext()).edit().putString(Constants.PREFS_LOGIN, edt_phone.getText().toString()).commit()) {
                    if (response.body() != null) {
                        if (response.body().getMessage() == null) {
                            Helper.jumpFragment(R.id.signIn_container, getActivity().getSupportFragmentManager(), FragmentInputSMS.newInstance(response.body().getData().getUser_id()), false).addToBackStack(null).commit();
                            Log.e("response", response.body().getResult() + "\n" + response.body().getData().getUser_id());
                        } else {
                            Log.e("response", response.body().getMessage() + " mess");
                        }
                    } else {
                        if (response.errorBody() != null) {
                            Gson gson = new GsonBuilder().create();
                            try {
                                Errors mApiError = gson.fromJson(response.errorBody().string(), Errors.class);
                                if (mApiError != null) {
                                    if (mApiError.getMessage().contains("exists")) {
                                        Toast.makeText(getActivity(), "Такой номер уже существует", Toast.LENGTH_SHORT).show();
                                    } else
                                        Toast.makeText(getActivity(), mApiError.getMessage() + "\n" + mApiError.getData(), Toast.LENGTH_SHORT).show();
                                } else
                                    Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_SHORT).show();

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.e("response", " null");
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<RegObject> call, Throwable t) {
                Log.e("errorRegister", t.toString());
            }
        });
    }


    private void resend() {
        if (objectCall != null)
            objectCall.cancel();
        Scheme scheme = ServiceGenerator.createService(Scheme.class, false);
        objectCall = scheme.resendCode(edt_phone.getText().toString());
        objectCall.enqueue(new Callback<RegObject>() {
            @Override
            public void onResponse(Call<RegObject> call, Response<RegObject> response) {
                if (response.body() != null) {
                    if (response.body().getMessage() == null) {
                        Helper.jumpFragment(R.id.signIn_container, getActivity().getSupportFragmentManager(), FragmentInputSMS.newInstance(response.body().getData().getUser_id()), false).addToBackStack(null).commit();
                        Log.e("response", response.body().getResult() + "\n" + response.body().getData().getUser_id());
                    } else {
                        Log.e("response", response.body().getMessage() + " mess");
                    }
                } else {
                    if (response.errorBody() != null) {
                        Gson gson = new GsonBuilder().create();
                        try {
                            Errors mApiError = gson.fromJson(response.errorBody().string(), Errors.class);
                            if (mApiError != null)
                                Toast.makeText(getActivity(), mApiError.getMessage() + "\n" + mApiError.getData(), Toast.LENGTH_SHORT).show();
                            else
                                Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_SHORT).show();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e("response", " null");
                    }
                }

            }

            @Override
            public void onFailure(Call<RegObject> call, Throwable t) {
                Log.e("errorRegister", t.toString());
            }
        });
    }
}
