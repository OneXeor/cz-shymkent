package onexeor.com.searchjob.fragments.SignInFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import onexeor.com.searchjob.Helper;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.SignInActivity;
import onexeor.com.searchjob.fragments.BaseFragment;

/**
 * Created by onexeor on 07.01.17.
 */

public class FragmentSignIn extends BaseFragment implements View.OnClickListener {

    private View rootView;
    private Button btn_reg, btn_login;

    public static FragmentSignIn newInstance() {
        FragmentSignIn fragmentSignIn = new FragmentSignIn();
        return fragmentSignIn;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_signin, container, false);
        btn_reg = (Button) rootView.findViewById(R.id.btn_reg);
        btn_login = (Button) rootView.findViewById(R.id.btn_login);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionBar = ((SignInActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Вход");
        }

        btn_login.setOnClickListener(this);
        btn_reg.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        btn_reg.setEnabled(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_reg:
                btn_reg.setEnabled(false);
                Helper.jumpFragment(R.id.signIn_container, getActivity().getSupportFragmentManager(), FragmentChRegMode.newInstance(), false)
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.btn_login:
                Helper.jumpFragment(R.id.signIn_container, getActivity().getSupportFragmentManager(), FragmentLogin.newInstance(), false)
                        .addToBackStack(null)
                        .commit();
                break;
        }
    }
}
