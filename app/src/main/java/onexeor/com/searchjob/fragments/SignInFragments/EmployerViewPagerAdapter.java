package onexeor.com.searchjob.fragments.SignInFragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import onexeor.com.searchjob.api.models.RegObject;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments.FragmentPageDescAgency;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments.FragmentPageNewsAgency;

/**
 * Created by OneXeor on 17.01.17
 * In 1:56.
 */

public class EmployerViewPagerAdapter extends FragmentStatePagerAdapter {

    private RegObject regObject;

    public EmployerViewPagerAdapter(RegObject regObject, FragmentManager fm) {
        super(fm);
        this.regObject = regObject;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FragmentPageDescAgency.newInstance(regObject);
            case 1:
                return FragmentPageNewsAgency.newInstance();
            default:
                return FragmentPageDescAgency.newInstance(regObject);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Информация о профиле";
            case 1:
                return "Новости проекта";
        }
        return super.getPageTitle(position);
    }

    @Override
    public int getCount() {
        return 2;
    }
}
