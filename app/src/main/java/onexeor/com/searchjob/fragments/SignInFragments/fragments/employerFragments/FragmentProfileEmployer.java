package onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import onexeor.com.searchjob.Constants;
import onexeor.com.searchjob.Helper;
import onexeor.com.searchjob.MainActivity;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.api.Scheme;
import onexeor.com.searchjob.api.ServiceGenerator;
import onexeor.com.searchjob.api.models.RegObject;
import onexeor.com.searchjob.fragments.BaseFragment;
import onexeor.com.searchjob.fragments.SignInFragments.EmployerViewPagerAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by OneXeor on 08.01.17
 * In 1:18 o'clock.
 */

public class FragmentProfileEmployer extends BaseFragment implements View.OnClickListener {

    private TabLayout tabs;
    private ViewPager viewPager;
    private TextView tv_name_agency, sub_tv_name;

    public static FragmentProfileEmployer newInstance() {
        return new FragmentProfileEmployer();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile_employer, container, false);
        tabs = (TabLayout) rootView.findViewById(R.id.tabs);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        tv_name_agency = (TextView) rootView.findViewById(R.id.tv_name_agency);
        sub_tv_name = (TextView) rootView.findViewById(R.id.sub_desc);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Профиль");
        }


        getUserdata();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    private void getUserdata() {
        String str_phone = Helper.getSPreferenceLogin(getActivity()).getString(Constants.PREFS_PHONE_NUMBER, "").replace("+", "").replaceAll("\\s+", "").replaceAll("-", "");
        String str_pass = Helper.getSPreferenceLogin(getActivity()).getString(Constants.PREFS_PASSWORD, "");
        String str_id = Helper.getSPreferenceLogin(getActivity()).getString(Constants.PREFS_USER_ID, "");

        Scheme scheme = ServiceGenerator.createServices(Scheme.class, str_phone, str_pass);
        Call<RegObject> call = scheme.getAnket(str_id);
        Log.e("ListArgs", call.request().url() + "\n" +
                str_id + " " + str_pass + "\n" + str_phone);
        call.enqueue(new Callback<RegObject>() {
            @Override
            public void onResponse(Call<RegObject> call, Response<RegObject> response) {
                if (response.body() != null) {
                    Log.e("response", response.body().getCompany_name() + "\n");

                    sub_tv_name.setText((response.body().getDescription() != null ? response.body().getDescription() : ""));
                    tv_name_agency.setText(response.body().getCompany_name());
                    EmployerViewPagerAdapter adapter = new EmployerViewPagerAdapter(response.body(), getChildFragmentManager());
                    viewPager.setAdapter(adapter);
                    tabs.setupWithViewPager(viewPager);
                }
                if (response.errorBody() != null) {
                    Gson gson = new GsonBuilder().create();
                    try {
                        RegObject mApiError = gson.fromJson(response.errorBody().string(), RegObject.class);
                        if (mApiError != null)
                            Log.e("Response", mApiError.getMessage() + " ");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RegObject> call, Throwable t) {
                Log.e("response", t.toString());
            }
        });
    }
}
