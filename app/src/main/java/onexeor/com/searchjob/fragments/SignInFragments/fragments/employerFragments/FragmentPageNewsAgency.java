package onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import onexeor.com.searchjob.adapters.NewsAdapter;
import onexeor.com.searchjob.NewsAgencyModel;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.fragments.BaseFragment;

/**
 * Created by OneXeor on 17.01.17
 * In 2:02.
 */

public class FragmentPageNewsAgency extends BaseFragment {


    private RecyclerView recyclerView;

    public static FragmentPageNewsAgency newInstance() {
        return new FragmentPageNewsAgency();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_page_news_agency, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);

        List<NewsAgencyModel> agencyModels = new ArrayList<>();
        agencyModels.add(new NewsAgencyModel("Мы запустились", "10.10.2016", "Задача организации, в особенности же новая модель организационной деятельности позволяет оценить значение дальнейших направлений развития. Повседневная практика показывает, что сложившаяся структура организации влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации соответствующий условий активизации. Задача организации, в особенности же реализация намеченных плановых заданий требуют от нас анализа системы обучения кадров, соответствует насущным потребностям. Таким образом дальнейшее развитие различных форм деятельности в значительной степени обуславливает создание системы обучения кадров, соответствует насущным потребностям."));
        agencyModels.add(new NewsAgencyModel("Новая версия. Новые улучшения.", "29.10.2016", "Задача организации, в особенности же новая модель организационной деятельности позволяет оценить значение дальнейших направлений развития. Повседневная практика показывает, что сложившаяся структура организации влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации соответствующий условий активизации. Задача организации, в особенности же реализация намеченных плановых заданий требуют от нас анализа системы обучения кадров, соответствует насущным потребностям. Таким образом дальнейшее развитие различных форм деятельности в значительной степени обуславливает создание системы обучения кадров, соответствует насущным потребностям."));

        NewsAdapter adapter = new NewsAdapter(getActivity(), agencyModels);
        recyclerView.setAdapter(adapter);
    }
}
