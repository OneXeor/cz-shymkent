package onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import onexeor.com.searchjob.Constants;
import onexeor.com.searchjob.Helper;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.api.models.RegObject;
import onexeor.com.searchjob.fragments.BaseFragment;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.applicantFragments.FragmentAnketaApplicant;

/**
 * Created by OneXeor on 17.01.17
 * In 2:02.
 */

public class FragmentPageDescAgency extends BaseFragment implements View.OnClickListener {

    private TextView tv_company_name, tv_sfera_agency, tv_inn_agency, tv_phone_agency;
    private RatingBar ratingBar;
    private FloatingActionButton fab_edit;
    private RegObject item;

    public static FragmentPageDescAgency newInstance(RegObject regObject) {
        FragmentPageDescAgency fragmentPageDescAgency = new FragmentPageDescAgency();
        Bundle bundle = new Bundle();
        bundle.putParcelable("anketa", regObject);
        fragmentPageDescAgency.setArguments(bundle);
        return fragmentPageDescAgency;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_page_desc_agency, container, false);
        tv_company_name = (TextView) rootView.findViewById(R.id.tv_company_name);
        tv_sfera_agency = (TextView) rootView.findViewById(R.id.tv_sfera_agency);
        tv_inn_agency = (TextView) rootView.findViewById(R.id.tv_inn_agency);
        tv_phone_agency = (TextView) rootView.findViewById(R.id.tv_phone_agency);
        ratingBar = (RatingBar) rootView.findViewById(R.id.rating_employer);
        fab_edit = (FloatingActionButton) rootView.findViewById(R.id.fab_edit);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            item = bundle.getParcelable("anketa");

            tv_company_name.setText(item.getCompany_name());
            tv_inn_agency.setText(item.getInn());
            tv_sfera_agency.setText(item.getActivity());
            tv_phone_agency.setText(item.getPhone());
            fab_edit.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_edit:
                Helper.jumpFragment(R.id.container, getActivity().getSupportFragmentManager(), FragmentAnketaEmployer.newInstance(item, true), false).addToBackStack(null).commit();
                break;
        }
    }
}
