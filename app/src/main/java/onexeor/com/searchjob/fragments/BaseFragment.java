package onexeor.com.searchjob.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;

import onexeor.com.searchjob.Constants;
import onexeor.com.searchjob.Helper;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.SignInActivity;
import onexeor.com.searchjob.fragments.SignInFragments.fragments.FragmentContacts;

/**
 * Created by onexeor on 07.01.17.
 */

public class BaseFragment extends Fragment {

    protected boolean isNetworkConnected() {
        if (getActivity() != null) {
            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null;
        }
        return true;
    }

    protected Dialog showDialog(String message) {
        return new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .create();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.contacts, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.contacts:
                Helper.jumpFragment(R.id.container, getActivity().getSupportFragmentManager(), FragmentContacts.newInstance(), false).addToBackStack(null).commit();
                return true;
            case R.id.exit:
                onCreateDialogExit().show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        hideKeyboard();
    }

    //Диалог выхода из приложения
    public Dialog onCreateDialogExit() {
        return new MaterialDialog.Builder(getActivity())
                .title(R.string.exit)
                .positiveText(R.string.yes)
                .content("Вы действительно хотите выйти из аккаунта?")
                .theme(Theme.LIGHT)
                .negativeText(R.string.no)
                .negativeColor(Color.parseColor("#8b000000"))
                .positiveColor(getResources().getColor(R.color.colorAccent))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (getActivity().getSharedPreferences(Constants.PREFS_LOGIN, Context.MODE_PRIVATE).edit().clear().commit()) {
                            getActivity().startActivity(new Intent(getActivity(), SignInActivity.class));
                            getActivity().finish();
                        }
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                }).build();
    }

    public void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }
}
