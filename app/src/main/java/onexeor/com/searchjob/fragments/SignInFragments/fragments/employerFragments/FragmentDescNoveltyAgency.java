package onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import onexeor.com.searchjob.MainActivity;
import onexeor.com.searchjob.NewsAgencyModel;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.fragments.BaseFragment;

/**
 * Created by OneXeor on 08.01.17
 * In 1:18 o'clock.
 */

public class FragmentDescNoveltyAgency extends BaseFragment implements View.OnClickListener {

    private TextView tv_desc_novelty, tv_title_novelty, tv_date_novelty;

    public static FragmentDescNoveltyAgency newInstance(NewsAgencyModel model) {
        FragmentDescNoveltyAgency agency = new FragmentDescNoveltyAgency();
        Bundle bundle = new Bundle();
        bundle.putParcelable("item", model);
        agency.setArguments(bundle);
        return agency;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_desc_novelty_agency, container, false);
        tv_date_novelty = (TextView) rootView.findViewById(R.id.tv_date_novelty);
        tv_desc_novelty = (TextView) rootView.findViewById(R.id.tv_desc_novelty);
        tv_title_novelty = (TextView) rootView.findViewById(R.id.tv_title_novelty);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Новости проекта");
        }

        Bundle bundle = getArguments();
        if (bundle != null) {
            NewsAgencyModel model = bundle.getParcelable("item");
            tv_title_novelty.setText(model.getTitle());
            tv_date_novelty.setText(model.getDate());
            tv_desc_novelty.setText(model.getDesc());
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }
}
