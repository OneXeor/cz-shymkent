package onexeor.com.searchjob.fragments.SignInFragments.fragments.employerFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import onexeor.com.searchjob.Constants;
import onexeor.com.searchjob.Helper;
import onexeor.com.searchjob.MainActivity;
import onexeor.com.searchjob.R;
import onexeor.com.searchjob.api.Scheme;
import onexeor.com.searchjob.api.ServiceGenerator;
import onexeor.com.searchjob.api.models.RegObject;
import onexeor.com.searchjob.api.models.Vacancy;
import onexeor.com.searchjob.fragments.BaseFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by OneXeor on 08.01.17
 * In 1:18 o'clock.
 */

public class FragmentAddVacancy extends BaseFragment implements View.OnClickListener {


    private EditText edt_name_vac, edt_time_mera_vac, edt_coast_money_, edt_count_worker, edt_address_vac, edt_desc_vac;
    private Button btn_send_vacancy;
    private Vacancy vacancy;

    public static FragmentAddVacancy newInstance(Vacancy vacancy) {
        FragmentAddVacancy fragmentAddVacancy = new FragmentAddVacancy();
        Bundle bundle = new Bundle();
        bundle.putParcelable("vacancy", vacancy);
        fragmentAddVacancy.setArguments(bundle);
        return fragmentAddVacancy;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_vacancy, container, false);
        edt_desc_vac = (EditText) rootView.findViewById(R.id.edt_desc_vac);
        edt_name_vac = (EditText) rootView.findViewById(R.id.edt_name_vac);
        edt_time_mera_vac = (EditText) rootView.findViewById(R.id.edt_time_mera_vac);
        edt_coast_money_ = (EditText) rootView.findViewById(R.id.edt_coast_money_);
        edt_count_worker = (EditText) rootView.findViewById(R.id.edt_count_worker);
        edt_address_vac = (EditText) rootView.findViewById(R.id.edt_address_vac);
        btn_send_vacancy = (Button) rootView.findViewById(R.id.btn_send_vacancy);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Добавить вакансию");
        }

        Bundle bundle = getArguments();
        vacancy = bundle.getParcelable("vacancy");


        btn_send_vacancy.setOnClickListener(this);
        if (vacancy != null) {
            btn_send_vacancy.setText("Сохранить");
            edt_count_worker.setText(vacancy.getWorkplaces());
            edt_address_vac.setText(vacancy.getAddress_go());
            edt_coast_money_.setText(vacancy.getSalary());
            edt_name_vac.setText(vacancy.getTitle().getRendered());
            edt_desc_vac.setText(vacancy.getContent().getRendered());
            edt_time_mera_vac.setText(vacancy.getTime_interval());
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send_vacancy:
                if (vacancy != null)
                    editVacancy();
                else
                    addVacancy();
                break;
        }
    }

    private void editVacancy() {
        String str = Helper.getSPreferenceLogin(getActivity()).getString(Constants.PREFS_PHONE_NUMBER, "12124123").replace("+", "").replaceAll("-", "").replaceAll("\\s+", "");
        String str_pass = Helper.getSPreferenceLogin(getActivity()).getString(Constants.PREFS_PASSWORD, "12124123");
        Log.e("phone", str.replace("+", "").replaceAll("-", "").replaceAll("\\s+", "") + "dsada ");
        Scheme scheme = ServiceGenerator.createServices(Scheme.class, str, str_pass);
        Call<Vacancy> call = scheme.editVacancy(vacancy.getId(), edt_name_vac.getText().toString(), edt_time_mera_vac.getText().toString(),
                edt_coast_money_.getText().toString(), "", edt_count_worker.getText().toString(), edt_address_vac.getText().toString(), "publish",
                edt_name_vac.getText().toString(), edt_desc_vac.getText().toString());
        call.enqueue(new Callback<Vacancy>() {
            @Override
            public void onResponse(Call<Vacancy> call, Response<Vacancy> response) {
                if (response.body() != null) {
                    Log.e("response", response.body().getId() + "");
                    getActivity().recreate();
                }
                if (response.errorBody() != null) {
                    Gson gson = new GsonBuilder().create();
                    try {
                        RegObject mApiError = gson.fromJson(response.errorBody().string(), RegObject.class);
                        if (mApiError != null) {
                            Log.e("Response", mApiError.getMessage() + " ");
                            if (getActivity() != null)
                                Toast.makeText(getActivity(), mApiError.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Vacancy> call, Throwable t) {
                Log.e("response", "response is null" + t.toString());

            }
        });
    }

    private void addVacancy() {
        String str = Helper.getSPreferenceLogin(getActivity()).getString(Constants.PREFS_PHONE_NUMBER, "12124123").replace("+", "").replaceAll("-", "").replaceAll("\\s+", "");
        String str_pass = Helper.getSPreferenceLogin(getActivity()).getString(Constants.PREFS_PASSWORD, "12124123");
        Log.e("phone", str + " ");
        Scheme scheme = ServiceGenerator.createServices(Scheme.class, str, str_pass);
        Call<Vacancy> call = scheme.addVacancy(edt_name_vac.getText().toString(), edt_time_mera_vac.getText().toString(),
                edt_coast_money_.getText().toString(), "", edt_count_worker.getText().toString(), edt_address_vac.getText().toString(), "publish",
                edt_name_vac.getText().toString(), edt_desc_vac.getText().toString());
        call.enqueue(new Callback<Vacancy>() {
            @Override
            public void onResponse(Call<Vacancy> call, Response<Vacancy> response) {
                if (response.body() != null) {
                    Log.e("response", response.body().getId() + "");
                    getActivity().onBackPressed();
                }
                if (response.errorBody() != null) {
                    Gson gson = new GsonBuilder().create();
                    try {
                        RegObject mApiError = gson.fromJson(response.errorBody().string(), RegObject.class);
                        if (mApiError != null) {
                            Log.e("Response", mApiError.getMessage() + " ");
                            if (getActivity() != null)
                                Toast.makeText(getActivity(), mApiError.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Vacancy> call, Throwable t) {
                Log.e("response", "response is null" + t.toString());

            }
        });
    }
}
